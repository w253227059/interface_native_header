/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup rawfile
 * @{
 *
 * @brief 提供操作rawfile目录和rawfile文件功能
 *
 * 功能包括遍历、打开、搜索、读取和关闭rawfile
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file raw_file.h
 *
 * @brief 提供rawfile文件相关功能
 *
 * 功能包括搜索、读取和关闭rawfile文件
 *
 * @since 8
 * @version 1.0
 */
#ifndef GLOBAL_RAW_FILE_H
#define GLOBAL_RAW_FILE_H

#include <string>

#ifdef __cplusplus
extern "C" {
#endif

struct RawFile;

/**
 * @brief 提供对rawfile的访问功能
 *
 *
 *
 * @since 8
 * @version 1.0
 */
typedef struct RawFile RawFile;

/**
 * @brief 提供rawfile文件描述符信息
 *
 * RawFileDescriptor是{@link OH_ResourceManager_GetRawFileDescriptor}的输出参数,
 * 涵盖了rawfile文件的文件描述符以及在HAP包中的起始位置和长度。
 *
 * @since 8
 * @version 1.0
 */
typedef struct {
    /** rawfile文件描述符 */
    int fd;

    /** rawfile在HAP包中的起始位置 */
    long start;

    /** rawfile在HAP包中的长度 */
    long length;
} RawFileDescriptor;

/**
 * @brief 读取rawfile
 *
 * 从当前位置读取<b>指定长度</b>的数据
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @param buf 用于接收读取数据的缓冲区指针
 * @param length 读取数据的字节长度
 * @return 返回读取的字节数，如果读取长度超过文件末尾长度，则返回<b>0</b>
 * @since 8
 * @version 1.0
 */
int OH_ResourceManager_ReadRawFile(const RawFile *rawFile, void *buf, size_t length);

/**
 * @brief 基于指定的offset，在rawfile文件内搜索读写数据的位置
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @param offset 表示指定的offset
 * @param whence 读写位置，有以下场景: \n
 * <b>0</b>: 读写位置为<b>offset</b> \n
 * <b>1</b>: 读写位置为当前位置加上<b>offset</b> \n
 * <b>2</b>: 读写位置为文件末尾(EOF)加上<b>offset</b>
 * @return 如果搜索成功返回新的读写位置，如果发生错误返回 <b>(long) -1</b>
 * @since 8
 * @version 1.0
 */
int OH_ResourceManager_SeekRawFile(const RawFile *rawFile, long offset, int whence);

/**
 * @brief 获取rawfile长度，单位为int32_t
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @return Returns rawfile整体长度
 * @since 8
 * @version 1.0
 */
long OH_ResourceManager_GetRawFileSize(RawFile *rawFile);

/**
 * @brief 关闭已打开的{@link RawFile} 以及释放所有相关联资源
 *
 *
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @see OH_ResourceManager_OpenRawFile
 * @since 8
 * @version 1.0
 */
void OH_ResourceManager_CloseRawFile(RawFile *rawFile);

/**
 * @brief 获取rawfile当前的offset，单位为int32_t
 *
 * rawfile当前的offset
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @return 返回rawfile当前的offset
 * @since 8
 * @version 1.0
 */
long OH_ResourceManager_GetRawFileOffset(const RawFile *rawFile);

/**
 * @brief 基于offset(单位为int32_t)和文件长度打开rawfile，并获取rawfile文件描述符
 *
 * 打开的文件描述符被用于读取rawfile
 *
 * @param rawFile 表示指向{@link RawFile}的指针
 * @param descriptor 显示rawfile文件描述符，以及在HAP包中的起始位置和长度
 * @return 返回true表示打开rawfile文件描述符成功，返回false表示rawfile不允许被访问
 * @since 8
 * @version 1.0
 */
bool OH_ResourceManager_GetRawFileDescriptor(const RawFile *rawFile, RawFileDescriptor &descriptor);

/**
 * @brief 关闭rawfile文件描述符
 *
 * 已打开的文件描述符在使用完以后必须释放，防止文件描述符泄露
 *
 * @param descriptor 包含rawfile文件描述符，以及在HAP包中的起始位置和长度
 * @return 返回true表示关闭文件描述符成功，返回false表示关闭文件描述符失败
 * @since 8
 * @version 1.0
 */
bool OH_ResourceManager_ReleaseRawFileDescriptor(const RawFileDescriptor &descriptor);

#ifdef __cplusplus
};
#endif

/** @} */
#endif // GLOBAL_RAW_FILE_H
