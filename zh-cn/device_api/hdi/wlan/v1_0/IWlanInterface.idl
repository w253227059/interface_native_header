/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * 上层WLAN服务开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描/关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IWlanInterface.idl
 *
 * @brief 建立/关闭WLAN热点，扫描/关联/去关联WLAN热点，设置国家码，管理网络设备等操作的接口。
 *
 * 上层服务调用相关的接口实现：建立/关闭WLAN热点，扫描/关联/去关联WLAN热点，设置国家码，管理网络设备等功能。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief WLAN模块接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.wlan.v1_0;

import ohos.hdi.wlan.v1_0.WlanTypes;
import ohos.hdi.wlan.v1_0.IWlanCallback;

/**
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * 上层服务调用相关的接口，可以建立/关闭WLAN热点，扫描/关联/去关联WLAN热点，设置国家码，管理网络设备等。
 *
 * @since 3.2
 * @version 1.0
 */

interface IWlanInterface {
    /**
     * @brief 创建HAL和驱动之间的通道及获取驱动网卡信息，该函数调用在创建IWiFi实体后进行。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Start();

    /**
     * @brief 销毁HAL和驱动之间的通道，该函数调用在销毁IWiFi实体前进行。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Stop();

    /**
     * @brief 根据输入类型创建对应的feature对象。
     *
     * @param type 创建的feature类型。
     * @param ifeature 获取创建的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    CreateFeature([in] int type, [out] struct HdfFeatureInfo ifeature);

    /**
     * @brief 销毁feature对象。
     *
     * @param ifeature 销毁的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    DestroyFeature([in] struct HdfFeatureInfo ifeature);

    /**
     * @brief 获取与AP连接的所有STA的信息（目前只包含MAC地址）。
     *
     * @param ifeature feature对象。
     * @param staInfo 保存与AP连接的STA的基本信息。
     * @param num 实际连接的STA的个数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetAsscociatedStas([in] struct HdfFeatureInfo ifeature, [out] struct HdfStaInfo[] staInfo, [out] unsigned int num);

    /**
     * @brief 获得当前驱动的芯片ID。
     *
     * @param ifeature feature对象。
     * @param chipId 获得的芯片ID。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */    
    GetChipId([in] struct HdfFeatureInfo ifeature, [out] unsigned char chipId);

    /**
     * @brief 获取设备的MAC地址。
     *
     * @param ifeature feature对象。
     * @param mac 获得的MAC地址。
     * @param len mac数组的长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetDeviceMacAddress([in] struct HdfFeatureInfo ifeature, [out] unsigned char[] mac, [in] unsigned char len);

    /**
     * @brief 通过网卡名称获取对应的feature对象。
     *
     * @param ifName 网卡名称。
     * @param ifeature 获取该网卡的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetFeatureByIfName([in] String ifName, [out] struct HdfFeatureInfo ifeature);

    /**
     * @brief 获取feature对象的类型。
     *
     * @param ifeature feature对象。
     * @param featureType feature对象的类型。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetFeatureType([in] struct HdfFeatureInfo ifeature, [out] int featureType);

    /**
     * @brief 获取指定频段（2.4G或者5G）下支持的频率。
     *
     * @param ifeature feature对象。
     * @param wifiInfo 频段信息。
     * @param freq 保存支持的频率。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetFreqsWithBand([in] struct HdfFeatureInfo ifeature, [in] struct HdfWifiInfo wifiInfo, [out] int[] freq);

    /**
     * @brief 通过芯片ID获得当前芯片所有的网卡名称。
     *
     * @param chipId 需要获取网卡名称的芯片ID。
     * @param ifNames 网卡名称。
     * @param num 网卡的数量。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetIfNamesByChipId([in] unsigned char chipId, [out] String ifName, [out] unsigned int num);

    /**
     * @brief 根据feature对象获取网卡名称。
     *
     * @param ifeature feature对象。
     * @param ifName 网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetNetworkIfaceName([in] struct HdfFeatureInfo ifeature, [out] String ifName);

    /**
     * @brief 获取多网卡共存情况。
     *
     * @param combo 基于芯片的能力保存当前所有支持的多网卡共存情况（比如支持AP、STA、P2P等不同组合的共存）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetSupportCombo([out] unsigned long combo);

    /**
     * @brief 获取该设备支持的WLAN特性（不考虑当前的使用状态）。
     *
     * @param supType 保存当前设备支持的特性。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetSupportFeature([out] unsigned char[] supType);

    /**
     * @brief 注册IWiFi的回调函数，监听异步事件。
     *
     * @param cbFunc 注册的回调函数。
     * @param ifName 网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RegisterEventCallback([in] IWlanCallback cbFunc, [in] String ifName);

    /**
     * @brief 去注册IWiFi的回调函数。
    
     * @param cbFunc 去注册的回调函数。
     * @param ifName 网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    UnregisterEventCallback([in] IWlanCallback cbFunc, [in] String ifName);

    /**
     * @brief 重启指定芯片ID的WLAN驱动程序。
     *
     * @param chipId 需要进行重启驱动的芯片ID。
     * @param ifName 网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    ResetDriver([in] unsigned char chipId, [in] String ifName);

    /**
     * @brief 设置国家码（表示AP射频所在的国家，规定了AP射频特性，包括AP的发送功率、支持的信道等。其目的是为了使AP的射频特性符合不同国家或区域的法律法规要求）。
     *
     * @param ifeature feature对象。
     * @param code 设置的国家码。
     * @param len 国家码长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetCountryCode([in] struct HdfFeatureInfo ifeature, [in] String code, [in] unsigned int len);

    /**
     * @brief 根据传入参数设置对应网卡的MAC地址。
     *
     * @param ifeature feature对象。
     * @param mac 设置的MAC地址。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetMacAddress([in] struct HdfFeatureInfo ifeature, [in] unsigned char[] mac);

    /**
     * @brief 设置扫描单个MAC地址。
     *
     * @param ifeature feature对象。
     * @param scanMac 设置STA扫描的MAC地址。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetScanningMacAddress([in] struct HdfFeatureInfo ifeature, [in] unsigned char[] scanMac);

    /**
     * @brief 设置发射功率。
     *
     * @param ifeature feature对象。
     * @param power 设置的发射功率。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetTxPower([in] struct HdfFeatureInfo ifeature, [in] int power);

    /**
     * @brief 获取网络设备信息（设备索引、网卡名字、MAC等信息）。
     *
     * @param netDeviceInfoResult 输出参数，得到的网络设备信息。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetNetDevInfo([out] struct HdfNetDeviceInfoResult netDeviceInfoResult);

    /**
     * @brief 启动扫描。
     *
     * @param ifeature feature对象。
     * @param scan 扫描参数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    StartScan([in] struct HdfFeatureInfo ifeature, [in] struct HdfWifiScan scan);

    /**
     * @brief 获取正在使用的功率模式。
     *
     * @param ifeature feature对象。
     * @param mode 功率模式，包括睡眠模式（待机状态运行）、一般模式（正常额定功率运行）、穿墙模式（最大功率运行，提高信号强度和覆盖面积）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetPowerMode([in] struct HdfFeatureInfo ifeature, [out] unsigned char mode);

    /**
     * @brief 设置功率模式。
     *
     * @param ifeature feature对象。
     * @param mode 功率模式,包括睡眠模式（待机状态运行）、一般模式（正常额定功率运行）、穿墙模式（最大功率运行，提高信号强度和覆盖面积）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetPowerMode([in] struct HdfFeatureInfo ifeature, [in] unsigned char mode);

    /**
     * @brief 启动信道测量。
     *
     * @param ifName 网卡名称。
     * @param measChannelParam 信道测量参数（信道号、测量时间）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    [oneway] StartChannelMeas([in] String ifName, [in] struct MeasChannelParam measChannelParam);

    /**
     * @brief 获取信道测量结果。
     *
     * @param ifName 网卡名称。
     * @param measChannelResult 信道测量结果（信道号、信道负载、信道噪声）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    GetChannelMeasResult([in] String ifName, [out] struct MeasChannelResult measChannelResult);

    /**
     * @brief 设置投屏参数。
     *
     * @param ifName 网卡名称。
     * @param param 投屏参数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SetProjectionScreenParam([in] String ifName, [in] struct ProjectionScreenCmdParam param);

    /**
     * @brief 向驱动发送IO控制命令。
     *
     * @param ifName 网卡名称。
     * @param cmdId 命令ID。
     * @param paramBuf 命令内容。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    WifiSendCmdIoctl([in] String ifName, [in] int cmdId, [in] byte[] paramBuf);
}
/** @} */