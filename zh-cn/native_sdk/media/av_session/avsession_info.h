/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_AVSESSION_INFO_H
#define OHOS_AVSESSION_INFO_H

/**
 * @addtogroup avsession
 * @{
 *
 * @brief 音视频媒体会话，提供系统内媒体的统一控制能力。
 *
 * 功能包括媒体会话，媒体会话管理，媒体会话控制。
 *
 * @syscap SystemCapability.Multimedia.AVSession.Core
 * @since 9
 * @version 1.0
 */

/**
 * @file avsession_info.h
 *
 * @brief 定义了与avsession相关的监听器以及回调功能的实现。
 *
 * @since 9
 * @version 1.0
 */

#include <string>
#include "avmeta_data.h"
#include "avplayback_state.h"
#include "avsession_descriptor.h"
#include "key_event.h"

namespace OHOS::AVSession {
/** AVSession死亡回调 */
using DeathCallback = std::function<void()>;

/**
 * @brief 定义与AVSession相关监听器的类的实现。
 *
 * @since 9
 * @version 1.0
 */
class SessionListener {
public:
    /**
     * @brief 创建AVSession会话的抽象的接口回调方法。
     *
     * @param descriptor AVSession的会话描述对象，类型为{@link AVSessionDescriptor}。
     * @see OnSessionRelease
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionCreate(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief 释放AVSession会话的抽象的接口回调方法。
     *
     * @param descriptor AVSession的会话描述对象，类型为{@link AVSessionDescriptor}。
     * @see OnSessionCreate
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionRelease(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief AVSession的TOP会话发生变化的抽象的接口回调方法。
     *
     * @param descriptor AVSession的会话描述对象，类型为{@link AVSessionDescriptor}。
     * @since 9
     * @version 1.0
     */
    virtual void OnTopSessionChanged(const AVSessionDescriptor& descriptor) = 0;

    /**
     * @brief SessionListener的默认的析构函数。
     *
     * @since 9
     * @version 1.0
     */
    virtual ~SessionListener() = default;
};

/**
 * @brief 定义AVSession回调类的实现
 *
 * @since 9
 * @version 1.0
 */
class AVSessionCallback {
public:
    /**
     * @brief AVSession多媒体播放的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlay() = 0;

    /**
     * @brief AVSession多媒体播放暂停的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPause() = 0;

    /**
     * @brief AVSession多媒体播放停止的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnStop() = 0;

    /**
     * @brief AVSession播放下一首多媒体的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlayNext() = 0;

    /**
     * @brief AVSession播放上一首多媒体的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnPlayPrevious() = 0;

    /**
     * @brief AVSession快进播放多媒体的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnFastForward() = 0;

    /**
     * @brief AVSession多媒体快退的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnRewind() = 0;

    /**
     * @brief AVSession多媒体跳播操作的抽象的回调方法。
     *
     * @param time 媒体资源的位置，从媒体资源开头开始计算，单位为ms。取值需大于等于0。
     * @since 9
     * @version 1.0
     */
    virtual void OnSeek(int64_t time) = 0;

    /**
     * @brief AVSession设置多媒体倍速播放操作的抽象的回调方法。
     *
     * @param speed 多媒体播放的倍速值。
     * @since 9
     * @version 1.0
     */
    virtual void OnSetSpeed(double speed) = 0;

    /**
     * @brief AVSession设置多媒体循环播放模式的抽象的回调方法。
     *
     * @param loopMode 多媒体循环播放模式，
     * 范围在 {@link AVPlaybackState#LOOP_MODE_SEQUENCE}到{@link AVPlaybackState#LOOP_MODE_SHUFFLE}之间。
     * @since 9
     * @version 1.0
     */
    virtual void OnSetLoopMode(int32_t loopMode) = 0;

    /**
     * @brief AVSession设置多媒体切换收藏操作的抽象的回调方法。
     *
     * @param mediald 多媒体ID号标识。
     * @since 9
     * @version 1.0
     */
    virtual void OnToggleFavorite(const std::string& mediald) = 0;

    /**
     * @brief AVSession多媒体按键事件处理的抽象的回调方法。
     *
     * @param keyEvent 按键事件码，类型为{@link MMI::KeyEvent}。
     * @since 9
     * @version 1.0
     */
    virtual void OnMediaKeyEvent(const MMI::KeyEvent& keyEvent) = 0;

    /**
     * @brief AVSessionCallback的默认的析构函数。
     *
     * @since 9
     * @version 1.0
     */
    virtual ~AVSessionCallback() = default;
};

/**
 * @brief 定义控制器相关回调操作的类的实现。
 *
 * @since 9
 * @version 1.0
 */
class AVControllerCallback {
public:
    /**
     * @brief AVSession会话销毁的抽象的回调方法。
     *
     * @since 9
     * @version 1.0
     */
    virtual void OnSessionDestroy() = 0;

    /**
     * @brief 音视频的播放状态发生改变的抽象的回调方法。
     *
     * @param state 音视频的播放状态的枚举值，类型为{@link AVPlaybackState}。
     * @since 9
     * @version 1.0
     */
    virtual void OnPlaybackStateChange(const AVPlaybackState &state) = 0;

    /**
     * @brief 会话元数据内容发生变化的抽象的回调方法。
     *
     * @param data 会话元数据内容，类型为{@link AVMetaData}。
     * @see AVMetaData
     * @since 9
     * @version 1.0
     */
    virtual void OnMetaDataChange(const AVMetaData &data) = 0;

    /**
     * @brief 当前会话激活状态发生改变的抽象的回调方法。
     *
     * @param isActive 表示是否激活。
     * @since 9
     * @version 1.0
     */
    virtual void OnActiveStateChange(bool isActive) = 0;

    /**
     * @brief 控制命令的有效性发生变化的抽象的回调方法。
     *
     * @param cmds，媒体有效的指令列表，范围为{@link #SESSION_CMD_INVALID}到{@link #SESSION_CMD_MAX}。
     * @since 9
     * @version 1.0
     */
    virtual void OnValidCommandChange(const std::vector<int32_t> &cmds) = 0;

    /**
     * @brief AVControllerCallback的默认的析构函数。
     *
     * @since 9
     * @version 1.0
     */
    virtual ~AVControllerCallback() = default;
};
} // namespace OHOS::AVSession
/** @} */
#endif // OHOS_AVSESSION_INFO_H